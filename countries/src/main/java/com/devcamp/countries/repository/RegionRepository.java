package com.devcamp.countries.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.countries.model.CRegion;

public interface RegionRepository extends JpaRepository<CRegion, Integer> {

}
