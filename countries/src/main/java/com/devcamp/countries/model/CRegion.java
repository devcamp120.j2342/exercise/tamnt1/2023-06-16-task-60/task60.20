package com.devcamp.countries.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

@Entity
@Table(name = "region")
public class CRegion {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "region_id")
	private long id;

	@Column(name = "region_code", unique = true)
	private String regionCode;

	@Column(name = "region_name")
	private String regionName;

	@ManyToOne
	@JoinColumn(name = "country_id", referencedColumnName = "country_id")
	private CCountry country;

	public CRegion() {
	}

	public CRegion(String regionCode, String regionName) {
		this.regionCode = regionCode;
		this.regionName = regionName;
	}

	public long getId() {
		return id;
	}

	public void setId(long region_id) {
		this.id = region_id;
	}

	public String getRegionCode() {
		return regionCode;
	}

	public void setRegionCode(String regionCode) {
		this.regionCode = regionCode;
	}

	public String getRegionName() {
		return regionName;
	}

	public void setRegionName(String regionName) {
		this.regionName = regionName;
	}

	public CCountry getCountry() {
		return country;
	}

	public void setCountry(CCountry country) {
		this.country = country;
	}

}
